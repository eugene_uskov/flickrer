//
//  SearchResultProvider.swift
//  Flikrer
//
//  Created by Eugene Uskov on 1/5/19.
//  Copyright © 2019 Eugene Uskov. All rights reserved.
//

import Foundation


typealias SearchResultsCompetion = (SearchResult)->()

protocol SearchResultsProvider {
    func search(for text: String, page: Int, completion: SearchResultsCompetion?)
}
