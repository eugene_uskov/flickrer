//
//  ImageCell.swift
//  Flikrer
//
//  Created by Eugene Uskov on 1/6/19.
//  Copyright © 2019 Eugene Uskov. All rights reserved.
//

import UIKit
import Kingfisher


// MARK: -
class ImageCell: UITableViewCell {
    @IBOutlet private weak var photoImageView: UIImageView!
    
    var flikrPhoto: FlikrPhoto?
    var downloadTask: RetrieveImageTask?
}


// MARK: - Methods
extension ImageCell {
    override func prepareForReuse() {
        super.prepareForReuse()
        
        downloadTask?.cancel()
        downloadTask = nil
    }
    
    func setup(forPhoto photo: FlikrPhoto) {
        flikrPhoto = photo
        
        guard photo.donwloadedImage == nil else {
            photoImageView.image = photo.donwloadedImage
            
            return
        }
        
        guard let url = URL(string: photo.imageURL) else { return }
        
        downloadTask = photoImageView.kf.setImage(
            with: url,
            placeholder: UIImage(named: "placeholder-image")
        ) { [weak self] image, error, _, _ in
            guard let image = image, error == nil else { return }
            
            self?.flikrPhoto?.donwloadedImage = image
            self?.photoImageView.image = image
            self?.downloadTask = nil
        }
    }
}

