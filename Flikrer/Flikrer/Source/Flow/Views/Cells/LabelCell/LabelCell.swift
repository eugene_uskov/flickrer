//
//  LabelCell.swift
//  Flikrer
//
//  Created by Eugene Uskov on 1/6/19.
//  Copyright © 2019 Eugene Uskov. All rights reserved.
//

import UIKit


// MARK: -
class LabelCell: UITableViewCell {
    @IBOutlet private weak var centerLabel: UILabel!
}


// MARK: - Methods
extension LabelCell {
    func setup(forText text: String) {
        centerLabel.text = text
    }
}

