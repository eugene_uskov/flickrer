//
//  ImagePreviewViewController.swift
//  Flikrer
//
//  Created by Eugene Uskov on 1/6/19.
//  Copyright © 2019 Eugene Uskov. All rights reserved.
//

import UIKit


// MARK: -
class ImagePreviewViewController: UIViewController {

    // MARK: - Outlet
    @IBOutlet private weak var imageView: UIImageView!

    
    // MARK: - Properties
    var photo: FlikrPhoto!
    
    var previousScale: CGFloat = 1
    var previousRotation: CGFloat = 1
}


// MARK: - Lifecycle
extension ImagePreviewViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setup()
    }
}


// MARK: - Setup
extension ImagePreviewViewController {
    private func setup() {
        setupImageView()
        setupGestureRecognizers()
    }
    
    private func setupImageView() {
        if let image = photo.donwloadedImage {
            imageView.image = image
        } else {
            guard let url = URL(string: photo.imageURL) else { return }
            
            imageView.kf.setImage(
                with: url,
                placeholder: UIImage(named: "placeholder-image")
            ) { [weak self] image, error, _, _ in
                guard let image = image, error == nil else { return }
                
                self?.photo?.donwloadedImage = image
                self?.imageView.image = image
            }
        }
    }
    
    private func setupGestureRecognizers() {
        let pinchGR = UIPinchGestureRecognizer(target: self, action: #selector(pinchView(_:)))
        let pinchRotateGR = UIRotationGestureRecognizer(target: self, action: #selector(rotateView(_:)))
        
        [pinchGR, pinchRotateGR].forEach { gr in
            gr.delegate = self
            imageView.addGestureRecognizer(gr)
        }
    }
}


// MARK: - GR Actions
extension ImagePreviewViewController {
    @objc func pinchView(_ pinchGR: UIPinchGestureRecognizer) {
        guard photo.donwloadedImage != nil else { return }
        
        previousScale = pinchGR.scale
        
        switch pinchGR.state {
        case .began, .changed:
            imageView.transform = .identity
            imageView.transform = imageView.transform.scaledBy(x: pinchGR.scale, y: pinchGR.scale).rotated(by: previousRotation)
            
        default: return
        }
    }
    
    @objc func rotateView(_ rotateGR: UIRotationGestureRecognizer) {
        guard photo.donwloadedImage != nil else { return }
        
        previousRotation = rotateGR.rotation
        
        switch rotateGR.state {
        case .began, .changed:
            imageView.transform = .identity
            imageView.transform = imageView.transform.rotated(by: rotateGR.rotation).scaledBy(x: previousScale, y: previousScale)
            
        default: return
        }
    }
}


// MARK: - UIGestureRecognizerDelegate
extension ImagePreviewViewController: UIGestureRecognizerDelegate {
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer,
                           shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
}

