//
//  ViewController.swift
//  Flikrer
//
//  Created by Eugene Uskov on 12/28/18.
//  Copyright © 2018 Eugene Uskov. All rights reserved.
//

import UIKit


// MARK: -
enum SearchState {
    case notSearched
    case searching
    case error
    case noResults
    case resultsFound([FlikrPhoto])
    
    var numberOfCells: Int {
        switch self {
        case .resultsFound(let images): return images.count
            
        default: return 1
        }
    }
    
    var textForLabel: String {
        switch self {
        case .noResults     : return "No results"
        case .notSearched   : return "Search for images"
        case .error         : return "Error occured"
            
        default             : return ""
        }
    }
    
    var heightForCell: CGFloat {
        switch self {
        case .error, .noResults, .notSearched, .searching   : return 60
        case .resultsFound(_)                               : return 165
        }
    }
}


// MARK: -
class SearchViewController: UIViewController {
    
    // MARK: - Outlets
    @IBOutlet private weak var tableView: UITableView!

    
    // MARK: - Properties
    private let searchResultsProvider: SearchResultsProvider = NetworkService()
    private var state: SearchState = .notSearched {
        didSet {
            tableView.reloadData()
        }
    }

    private let imageCellReuseIdentifier = "ImageCell"
    private let searchingCellReuseIdentifier = "SearchingCell"
    private let labelCellReuseIdentifier = "LabelCell"
}


// MARK: - Lifecycle
extension SearchViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setup()
    }
}


// MARK: - Setup
fileprivate extension SearchViewController {
    private func setup() {
        setupSearchBar()
        setupCollectionView()
    }
    
    private func setupSearchBar() {
        let sb = UISearchBar(frame: .zero)
        
        sb.delegate = self
        sb.enablesReturnKeyAutomatically = false
        sb.placeholder = "Search"
        
        navigationItem.titleView = sb
    }
    
    private func setupCollectionView() {
        let imageCellNib = UINib(nibName: "ImageCell", bundle: nil)
        let searchingCellNib = UINib(nibName: "SearchingCell", bundle: nil)
        let labelCellNib = UINib(nibName: "LabelCell", bundle: nil)

        tableView.register(imageCellNib, forCellReuseIdentifier: imageCellReuseIdentifier)
        tableView.register(searchingCellNib, forCellReuseIdentifier: searchingCellReuseIdentifier)
        tableView.register(labelCellNib, forCellReuseIdentifier: labelCellReuseIdentifier)
    }
}


// MARK: - UISearchBarDelegate
extension SearchViewController: UISearchBarDelegate {
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        state = .notSearched
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        guard !searchText.isEmpty else {
            state = .notSearched
            
            return
        }
        
        state = .searching
        
        searchResultsProvider.search(for: searchText, page: 1) { [weak self] result in
            switch result {
            case .success(let searchResult):
                guard searchResult.photos.count > 0 else {
                    self?.state = .noResults
                    
                    return
                }
                
                self?.state = .resultsFound(searchResult.photos)
                
            case .failure:
                self?.state = .error
            }
        }
    }
}


// MARK: - UITableViewDataSource
extension SearchViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return state.numberOfCells
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: UITableViewCell
        
        switch state {
        case .noResults, .notSearched, .error:
            cell = tableView.dequeueReusableCell(withIdentifier: labelCellReuseIdentifier, for: indexPath)
            (cell as? LabelCell)?.setup(forText: state.textForLabel)
            
        case .searching:
            cell = tableView.dequeueReusableCell(withIdentifier: searchingCellReuseIdentifier, for: indexPath)
            
        case .resultsFound(let images):
            cell = tableView.dequeueReusableCell(withIdentifier: imageCellReuseIdentifier, for: indexPath)
            (cell as? ImageCell)?.setup(forPhoto: images[indexPath.row])
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return state.heightForCell
    }
}


// MARK: - UITableViewDelegate
extension SearchViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        switch state {
        case .resultsFound(let images):
            performSegue(withIdentifier: "showImagePreview", sender: images[indexPath.row])

        default: return
        }
    }
}


// MARK: - Nsvigation
extension SearchViewController {
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard let photo = sender as? FlikrPhoto,
            let destination = segue.destination as? ImagePreviewViewController
            else { return }
           
        destination.photo = photo
    }
}


