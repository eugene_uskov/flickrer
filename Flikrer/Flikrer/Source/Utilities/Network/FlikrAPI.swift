//
//  FlikrAPI.swift
//  Flikrer
//
//  Created by Eugene Uskov on 1/5/19.
//  Copyright © 2019 Eugene Uskov. All rights reserved.
//

import Moya


// MARK: -
enum FlikrAPI {
    static private let privateKey = "1f090cecde96941b6abe273abd1229ee"
    static private let secret = "ce48a42e006a716c"
    
    case search(text: String, page: Int)
}


// MARK: - TargetType
extension FlikrAPI: TargetType {
    var baseURL: URL {
        return URL(string: "https://api.flickr.com/services/rest/")!
    }
    
    var path: String {
        switch self {
        case .search: return ""
        }
    }
    
    var method: Moya.Method {
        switch self {
        case .search: return .get
        }
    }
    
    var sampleData: Data {
        return Data()
    }
    
    var task: Task {
        switch self {
        case .search(text: let text, page: let page): return .requestParameters(parameters: [
            "method"   : "flickr.photos.search",
            "api_key"  : FlikrAPI.privateKey,
            "text"     : text,
            "page"     : page,
            "format"   : "json"
            ],
                                                                                encoding: URLEncoding.queryString)
        }
    }
    
    var headers: [String: String]? {
        return ["Content-Type": "application/json"]
    }
    
    var validationType: ValidationType {
        return .successCodes
    }
}
