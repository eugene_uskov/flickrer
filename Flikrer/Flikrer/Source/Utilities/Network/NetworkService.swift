//
//  FlikrSearcher.swift
//  Flikrer
//
//  Created by Eugene Uskov on 1/5/19.
//  Copyright © 2019 Eugene Uskov. All rights reserved.
//

import UIKit
import Moya


// MARK: -
class NetworkService {
    let provider = MoyaProvider<FlikrAPI>(plugins: [NetworkLoggerPlugin(verbose: true)])
    var lastRequest: Cancellable?
}


// MARK: - SearchResultProvider
extension NetworkService: SearchResultsProvider {
    func search(for text: String, page: Int = 1, completion: SearchResultsCompetion? = nil) {
        lastRequest?.cancel()
        
        lastRequest = provider.request(.search(text: text, page: page)) { result in
            switch result {
            case .success(let response):
                // For some reason here was decoding arror of invalid JSON without these lines
                let range = Range(uncheckedBounds: (14, response.data.count - 1))
                let newData = response.data.subdata(in: range)
                
                do {
                    let flikrResponse = try JSONDecoder().decode(FlikrSearchResponse.self, from: newData)
                    
                    completion?(.success(flikrResponse.result))
                } catch let error {
                    completion?(.failure(error))
                }
                
            case .failure(let error):
                guard error.localizedDescription != "cancelled" else { return }
                
                completion?(.failure(error))
            }
        }
    }
}

