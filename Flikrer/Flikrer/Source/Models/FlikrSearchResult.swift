//
//  FlikrImageResult.swift
//  Flikrer
//
//  Created by Eugene Uskov on 1/5/19.
//  Copyright © 2019 Eugene Uskov. All rights reserved.
//

import Foundation


// MARK: -
struct FlikrSearchResult {
    let page: Int
    let numberOfResults: Int
    let numberOfPages: Int
    let photos: [FlikrPhoto]
}


// MARK: - Decodable
extension FlikrSearchResult: Decodable {
    private enum CodingKeys: String, CodingKey {
        case page
        case numberOfResults = "perpage"
        case numberOfPages = "pages"
        case photos = "photo"
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        page = try container.decode(Int.self, forKey: .page)
        numberOfResults = try container.decode(Int.self, forKey: .numberOfResults)
        numberOfPages = try container.decode(Int.self, forKey: .numberOfPages)
        photos = try container.decode([FlikrPhoto].self, forKey: .photos)
    }
}
