//
//  SearchResult.swift
//  Flikrer
//
//  Created by Eugene Uskov on 1/5/19.
//  Copyright © 2019 Eugene Uskov. All rights reserved.
//

import UIKit


// MARK: -
enum SearchResult {
    case success(FlikrSearchResult)
    case failure(Error)
}
