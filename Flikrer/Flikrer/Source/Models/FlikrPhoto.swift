//
//  FlikrImage.swift
//  Flikrer
//
//  Created by Eugene Uskov on 1/5/19.
//  Copyright © 2019 Eugene Uskov. All rights reserved.
//

import UIKit


// MARK: -
class FlikrPhoto {
    var id: String = ""
    var farm: Int = 0
    var server: String = ""
    var secret: String = ""
    var imageURL: String = ""
    var donwloadedImage: UIImage? 
    
    init(id: String, farm: Int, server: String, secret: String) {
        self.id = id
        self.farm = farm
        self.server = server
        self.secret = secret
        self.imageURL = createImageURL(fromID: id, farm: farm, server: server, secret: secret)
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        id = try container.decode(String.self, forKey: .id)
        farm = try container.decode(Int.self, forKey: .farm)
        server = try container.decode(String.self, forKey: .server)
        secret = try container.decode(String.self, forKey: .secret)
        imageURL = createImageURL(fromID: id, farm: farm, server: server, secret: secret)
    }
    
    private func createImageURL(fromID id: String, farm: Int, server: String, secret: String) -> String {
        return "https://farm\(farm).staticflickr.com/\(server)/\(id)_\(secret)_h.jpg"
    }
}


// MARK: - Decodable
extension FlikrPhoto: Decodable {
    enum CodingKeys: String, CodingKey {
        case id
        case farm
        case server
        case secret
    }
}


