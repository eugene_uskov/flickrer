//
//  FlikrSearchResponse.swift
//  Flikrer
//
//  Created by Eugene Uskov on 1/5/19.
//  Copyright © 2019 Eugene Uskov. All rights reserved.
//

import Foundation


// MARK: -
struct FlikrSearchResponse {
    let result: FlikrSearchResult
}


// MARK: - Decodable
extension FlikrSearchResponse: Decodable {
    private enum CodingKeys: String, CodingKey {
        case result = "photos"
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        result = try container.decode(FlikrSearchResult.self, forKey: .result)
    }
}
